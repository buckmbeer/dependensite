﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DependenSite.Models;

namespace DependenSite.Controllers
{
	public class HomeController : Controller
	{
		public IActionResult Index()
		{
			var firstStringService = new FirstPrimaryDependency.FirstStringService();
			ViewData["LibraryOneString"] = firstStringService.GetStrings().First();
			var secondStringService = new SecondPrimaryDependency.SecondStringService();
			ViewData["LibraryTwoString"] = secondStringService.GetString();
			return View();
		}

		public IActionResult Privacy()
		{
			return View();
		}

		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		public IActionResult Error()
		{
			return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
		}
	}
}
